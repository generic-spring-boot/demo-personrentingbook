package generic.spring.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GenericSpringBootDemoPersonRentingBookApplication {

    public static void main(String[] args) {
        SpringApplication.run(GenericSpringBootDemoPersonRentingBookApplication.class, args);
    }

}
