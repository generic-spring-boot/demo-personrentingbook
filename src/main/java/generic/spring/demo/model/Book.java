package generic.spring.demo.model;


import generic.spring.annotations.Generic;
import generic.spring.demo.dto.BookDTO;
import generic.spring.generics.Mapper;
import generic.spring.generics.base.BaseEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.HashSet;
import java.util.Set;


@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Generic
@Entity
public class Book extends BaseEntity<Long> implements Mapper<BookDTO, Book> {
    private String isbn;
    private String title;
    private String author;
    private String genre;
    private Integer available;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "book")
    private Set<Rental> rentals = new HashSet<>();
}