package generic.spring.demo.model;


import generic.spring.annotations.Generic;
import generic.spring.demo.dto.PersonDTO;
import generic.spring.generics.Mapper;
import generic.spring.generics.base.BaseEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.HashSet;
import java.util.Set;


@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Generic
@Entity
public class Person extends BaseEntity<Long> implements Mapper<PersonDTO, Person> {
    private String username;
    private String name;
    private String surname;


    @OneToMany(fetch = FetchType.EAGER, mappedBy = "person")
    private Set<Rental> rentals = new HashSet<>();
}