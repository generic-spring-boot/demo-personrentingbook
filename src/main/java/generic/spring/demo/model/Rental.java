package generic.spring.demo.model;


import generic.spring.annotations.Generic;
import generic.spring.demo.dto.RentalDTO;
import generic.spring.generics.Mapper;
import generic.spring.generics.base.BaseEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;


@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Generic
@Entity
public class Rental extends BaseEntity<Long> implements Mapper<RentalDTO, Rental> {
    private LocalDateTime rented;
    private LocalDateTime returned;

    @ManyToOne
    private Person person;

    @ManyToOne
    private Book book;
}