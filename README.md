# Demo-PersonRentingBook

This is a demo project showcasing the capabilities
of [Generic Spring Boot](https://gitlab.com/Igor.Kostin/generic-spring-boot), a solution for rapidly developing fully
functional CRUD Spring Boot applications. With this approach, application development becomes data-driven, allowing
developers to focus on defining data models while automating the generation of common components like DTOs,
Repositories, Services, and Controllers.

## Getting Started

To get started with the demo project, follow these steps:

1. Follow the installation process for [Generic Spring Boot - Generics](https://gitlab.com/Igor.Kostin/generics) - in
   section #Installation.

   *Note* - In this case, you don't need to follow step 3 and beyond.

2. Follow the installation process
   for [Generic Spring Boot - Processor](https://gitlab.com/generic-spring-boot/processor).

3. `git clone https://gitlab.com/generic-spring-boot/demo-personrentingbook.git`.

4. In the newly created project, open the `application.yaml` file:

    1. Change `spring.datasource` properties to correspond to your local or remote environment.

    2. Change `jpa.properties.hibernate.dialect` with the corresponding dialect for your local MySQL/MariaDB server.

5. Change `pom.xml` in the section about repositories and add your own local repository for Maven dependencies.

6. Change or install the required Java version.

7. Build and run the server.

## Usage

You can use this server as you like or use the pre-built Postman Workspace for pre-configured requests to test
functionalities.

Postman Workspace ID : fa294afd-a1a8-47f9-9012-18d2ada082bc

# The Demo Project

This is a simple demo project showcasing the capabilities and functionalities of Generic Spring Boot. It demonstrates
how a fully functional server can be started and run simply by defining its models.

![Model Definition](assets/img.png)

However, there are some considerations when defining models:

You have to:

- Annotate with `@Generic`
- Let's say `MyAwesomeEntity`
- Must extend `BaseEntity<(ID Type)Long>`
- Implement `Mapper<MyAwesomeEntityDTO, MyAwesomeEntity`, allowing mapping between DTO and entity (DTO import path must
  be provided).

For example, when defining the `Book` entity:

![Book Entity Example](assets/img_1.png)

With these minor considerations, you can successfully create a CRUD Generic Spring Boot Application. :)
